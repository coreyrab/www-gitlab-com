---
title: "GitLab's Functional Group Updates - May 30th - June 2nd" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: GitLab news
description: "The Functional Groups at GitLab give an update on what they've been working on"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI Team

[Presentation slides](https://docs.google.com/presentation/d/1q1l5IU6u7r9Og1Zne3EmLw1aLHU2jbArwP8fWFeNGrs/edit#slide=id.g153a2ed090_0_57)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/dMYb_dC_KBk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Build Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1u1WY_7EUjQwkCGcbK3Phy9KXEe0Xn8_RYj5msG3awvs/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/UA9xyiFJYAk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Support Team

[Presentation slides](https://docs.google.com/presentation/d/1EizMPiTJFYm7R7Av6J7DguR_Crgo_t8pufLYKoGC5sU/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8ZyUAVIyKUo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Infrastructure Team

[Presentation slides](https://docs.google.com/presentation/d/1YWVg6DpgUdFk5E-6ZOkABspJfFrG8zjAmPZ5Vjcr8bA/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_Aj_VqadBvY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
