---
layout: markdown_page
title: Process
---

### Project Process
1. **Manager, Professional Services**: Once an SOW has been approved and moved for **Closed Won**, assign an Implementation Engineer.
1. **Manager, Professional Services**: Send [welcome e-mail](/handbook/customer-success/implementation-engineering/workflows/project_execution/welcome-email.html)
1. **Implementation Engineer**: Begins project with processes defined here.  
  - [Kick-off](/handbook/customer-success/implementation-engineering/workflows/project_execution/kick-off.html)
  - Intake for [AWS](/handbook/customer-success/implementation-engineering/workflows/intake/aws.html) or [on-prem](/handbook/customer-success/implementation-engineering/workflows/intake/on-prem.html)
  - [On-going project calls (external)](/handbook/customer-success/implementation-engineering/workflows/project_execution/calls.html)
  - [On-going internal project updates](/handbook/customer-success/implementation-engineering/workflows/internal/15minute-standup.html)
  - [Project Summary](http://localhost:4567/handbook/customer-success/implementation-engineering/workflows/project_execution/project-summary.html)
1. **Implementation Engineer**: Starts [financal wrap-up](/handbook/customer-success/implementation-engineering/workflows/internal/financial-wrapup.html) process.
1. **Manager, Professional Services**: Schedule [blameless post-mordem](/handbook/customer-success/implementation-engineering/workflows/internal/post-mortem.html)

### Quote to Sign-off Process
This is an overview of the entire process for a services engagement from discovery of the customer's needs to the sign-off from the customer.

See [Professional Services Business Operations](/handbook/customer-success/implementation-engineering/workflows/internal/biz-ops.html)